function [ Precision, Recall ] = CalculateWeightedPrecisionRecall( KnownFeatures, KnownLabels, UnKnownFeatures, UnKnownLabels)
SimMat = load('/lustre/koustav/structures/Misc/TUB_SimilarityMatrix.mat');
SimilarityMatrix = SimMat.conf;
%CALCULATEPRECISIONRECALL Summary of this function goes here
%   Detailed explanation goes here
Features = [KnownFeatures ; UnKnownFeatures];
Labels = [KnownLabels UnKnownLabels];
Labels = single(Labels);
DatasetSize = size(Features,1);
[ImageSampleCount,ImageUniqueLabel] = hist(single(Labels),unique(Labels));
[IDX,D] = knnsearch(Features,UnKnownFeatures,...
    'k',DatasetSize,'distance','euclidean');
PrecisionArray=[];RecallArray=[];
for i=1:size(IDX,1)
    thisLabel = UnKnownLabels(i);
    cc =0;rc=0;
    pr = [];re=[];
    for j =1:size(IDX,2)
        if (Labels(IDX(i,j)) == thisLabel)
            pc=pc+1;
            rc=rc+1;
        else
            pc=pc+SimilarityMatrix(thisLabel,Labels(IDX(i,j)));
        end
        pr(j) = pc/j;re(j) = rc/ImageSampleCount(find(thisLabel == ImageUniqueLabel));
       % pr(j) = cc;re(j) = cc/ImageSampleCount(thisSketchLabel);
            
    end
    PrecisionArray(i,:) = pr;RecallArray(i,:) = re;
end
Precision = mean(PrecisionArray,1);Recall = mean(RecallArray,1);
end

