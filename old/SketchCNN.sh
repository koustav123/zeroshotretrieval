#!/bin/bash
#SBATCH -A cvit
#SBATCH -n 4
#SBATCH -p long
#SBATCH --mem=15000
#SBATCH -t 24:00:00

/scratch/matlab/R2013b/bin/matlab -nodesktop -nosplash -singleCompThread -r "fn_Check_KNN_Accuracy_1('$1','$2');"
#/scratch/matlab/R2013b/bin/matlab -nodesktop -nosplash -singleCompThread -r "fn_Check_Weighted_KNN_Accuracy('$1');"
