clc;
%clear;
addpath('../Utilities/');
%SketchData = load('/lustre/koustav/structures/FullFeatures/tuberlinimdb.mat');
%ImageData = load('/lustre/koustav/structures/FullFeatures/caltech256imdb.mat');
SketchData = load('/lustre/koustav/structures/FullFeatures/TUB_Fisher_Full_SketchData.mat');
 Labels = SketchData.FileNames;
% Labels = Labels';
 Features = SketchData.Features;
 Features = ReduceDim(Features,1000,2);
 
 
% Features = SketchData.imdb.images.featuresfc2conv23;
% 
% 
Labels = single(Labels);
sampleIndexes = randi([1,size(Features,1)],[floor(size(Features,1)/5),1]);
tIdx = Labels; tIdx(sampleIndexes) = -1;
tIdx(tIdx~=-1) = 0;tIdx(tIdx == -1) = 1;
tIdx = logical(tIdx);

DatasetSize = size(Features,1);
[ImageSampleCount,ImageUniqueLabel] = hist(Labels,unique(Labels));
[IDX,D] = knnsearch(Features,Features(tIdx,:),...
    'k',DatasetSize,'distance','euclidean');

QueryLabels = Labels(tIdx);

PrecisionArray=[];RecallArray=[];
for i=1:size(IDX,1)
    disp(i);
    thisLabel = QueryLabels(i);
    cc =0;
    pr = [];re=[];
    for j =1:size(IDX,2)
        if (Labels(IDX(i,j)) == thisLabel)
            cc=cc+1;
        end
        pr(j) = cc/j;re(j) = cc/ImageSampleCount(find(thisLabel == ImageUniqueLabel));
       % pr(j) = cc;re(j) = cc/ImageSampleCount(thisSketchLabel);
            
    end
    PrecisionArray(i,:) = pr; RecallArray(i,:) = re;
end
Precision = mean(PrecisionArray,1);Recall = mean(RecallArray,1);

plot(Recall,Precision);title('Fisher Features');savefig([features '.png']);close all;