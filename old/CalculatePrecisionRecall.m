function [ Precision, Recall ] = CalculatePrecisionRecall( KnownFeatures, KnownLabels, UnKnownFeatures, UnKnownLabels)
%CALCULATEPRECISIONRECALL Summary of this function goes here
%   Detailed explanation goes here
Features = [KnownFeatures ; UnKnownFeatures];
Labels = [KnownLabels UnKnownLabels];
Labels = single(Labels);
DatasetSize = size(Features,1);
[ImageSampleCount,ImageUniqueLabel] = hist(Labels,unique(Labels));
[IDX,D] = knnsearch(Features,UnKnownFeatures,...
    'k',DatasetSize,'distance','euclidean');
PrecisionArray=[];RecallArray=[];
for i=1:size(IDX,1)
    thisLabel = UnKnownLabels(i);
    cc =0;
    pr = [];re=[];
    for j =1:size(IDX,2)
        if (Labels(IDX(i,j)) == thisLabel)
            cc=cc+1;
        end
        pr(j) = cc/j;re(j) = cc/ImageSampleCount(find(thisLabel == ImageUniqueLabel));
       % pr(j) = cc;re(j) = cc/ImageSampleCount(thisSketchLabel);
            
    end
    PrecisionArray(i,:) = pr; RecallArray(i,:) = re;
end
Precision = mean(PrecisionArray,1);Recall = mean(RecallArray,1);
end

