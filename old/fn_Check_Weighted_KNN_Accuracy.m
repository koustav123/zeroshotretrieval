function[] = fn_Check_KNN_Accuracy (features)
%clc;
%clear;
addpath('../Utilities/');
%SketchData = load('/lustre/koustav/structures/FullFeatures/tuberlinimdb.mat');
SketchData = load('/lustre/koustav/structures/FullFeatures/wordvec_net_features.mat');
%SketchData = load(path);
%Labels = SketchData.imdb.images.labels;
SimMat = load('/lustre/koustav/structures/Misc/TUB_SimilarityMatrix.mat');
SimilarityMatrix = SimMat.conf;
switch features
    case '1'
        Features = SketchData.imdb.images.featuresfc1relu16;
    case '2'
        Features = SketchData.imdb.images.featuresfc2relu19;
    case '3'
        Features = SketchData.imdb.images.featuresfc3conv21;
    case 'OldCNN'
        Features = SketchData.imdb.images.features;
end
Labels = load('/lustre/koustav/TUBerlin/sketchnewlabels.mat'); Labels = Labels.labels;
%Features = load(path); Features = Features.
%Features = ReduceDim(Features,1000,2);
 
Labels = single(Labels);
sampleIndexes = randi([1,size(Features,1)],[floor(size(Features,1)/5),1]);
tIdx = Labels; tIdx(sampleIndexes) = -1;
tIdx(tIdx~=-1) = 0;tIdx(tIdx == -1) = 1;
tIdx = logical(tIdx);

DatasetSize = size(Features,1);
[ImageSampleCount,ImageUniqueLabel] = hist(Labels,unique(Labels));
[IDX,D] = knnsearch(Features,Features(tIdx,:),...
    'k',DatasetSize,'distance','euclidean');

QueryLabels = Labels(tIdx);

PrecisionArray=[];RecallArray=[];
for i=1:size(IDX,1)
    disp(i);
    thisLabel = QueryLabels(i);
    pc =0;rc=0;
    pr = [];re=[];
    for j =1:size(IDX,2)
        if (Labels(IDX(i,j)) == thisLabel)
            pc=pc+1;
            rc=rc+1;
        else
            pc=pc+SimilarityMatrix(thisLabel,Labels(IDX(i,j)));
        end
        pr(j) = pc/j;re(j) = rc/ImageSampleCount(find(thisLabel == ImageUniqueLabel));
       % pr(j) = cc;re(j) = cc/ImageSampleCount(thisSketchLabel);
            
    end
    PrecisionArray(i,:) = pr; RecallArray(i,:) = re;
end
Precision = mean(PrecisionArray,1);Recall = mean(RecallArray,1);
disp(Precision);disp(Recall);
save(['Weighted_PrecisionRecall_' features '.mat'],'Precision','Recall');
%plot(Recall,Precision);title(features);savefig([features '.png']);close all;