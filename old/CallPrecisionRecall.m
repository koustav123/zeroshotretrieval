clc;clear;
%SketchData = load('/lustre/koustav/structures/FullFeatures/TUB_CNN_Full_SketchData.mat');
SketchData = load('/lustre/koustav/structures/FullFeatures/127fullfeaturesTUB80250.mat');
Labels = SketchData.imdb.images.labels;
Features = SketchData.imdb.images.featuresfc2conv23;

tIdx = randi([1,250],[50,1]);
y = logical(zeros(size(Labels)));

for j = 1:size(tIdx,1)
       y = bitor(y,Labels ==tIdx(j));
end

[Precision,Recall] = CalculatePrecisionRecall(Features(~y,:),Labels(~y),Features(y,:),Labels(y));