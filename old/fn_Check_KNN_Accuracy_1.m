function[] = fn_Check_KNN_Accuracy_1 (features,age)
%clc;
%clear;
addpath('../Utilities/');
SketchData = load('/lustre/koustav/structures/FullFeatures/epoch_356_6layers_816.mat');
%SketchData = load(path);
%Labels = SketchData.imdb.images.set;
switch age
    case 'simple'
        switch features
            case '1'
                Features = SketchData.imdb.images.simplefeaturesfc3convrelufc2convrelufc1convrelu(1,:,:);
            case '2'
                Features = SketchData.imdb.images.simplefeaturesfc3convrelufc2convrelufc1convrelu(2,:,:);
            case '3'
                Features = SketchData.imdb.images.simplefeaturesfc3convrelufc2convrelufc1convrelu(3,:,:);
            case '4'
                Features = SketchData.imdb.images.simplefeaturesfc3convrelufc2convrelufc1convrelu(4,:,:);
            case '5'
                Features = SketchData.imdb.images.simplefeaturesfc3convrelufc2convrelufc1convrelu(5,:,:);
            case '6'
                Features = SketchData.imdb.images.simplefeaturesfc3convrelufc2convrelufc1convrelu(6,:,:);
        end
    case 'complex'
        switch features
            case '1'
                Features = SketchData.imdb.images.complexfeaturesfc3convrelufc2convrelufc1convrelu(1,:,:);
            case '2'
                Features = SketchData.imdb.images.complexfeaturesfc3convrelufc2convrelufc1convrelu(2,:,:);
            case '3'
                Features = SketchData.imdb.images.complexfeaturesfc3convrelufc2convrelufc1convrelu(3,:,:);
            case '4'
                Features = SketchData.imdb.images.complexfeaturesfc3convrelufc2convrelufc1convrelu(4,:,:);
            case '5'
                Features = SketchData.imdb.images.complexfeaturesfc3convrelufc2convrelufc1convrelu(5,:,:);
            case '6'
                Features = SketchData.imdb.images.complexfeaturesfc3convrelufc2convrelufc1convrelu(6,:,:);
                
        end
        
end
Features = squeeze(Features);
Labels = SketchData.imdb.images.actlabels;
%Labels = load('/lustre/koustav/TUBerlin/sketchnewlabels.mat'); Labels = Labels.labels;
%Features = load(path); Features = Features.
%Features = ReduceDim(Features,1000,2);
Labels = single(Labels);
%sampleIndexes = randi([1,size(Features,1)],[floor(size(Features,1)/5),1]);
%tIdx = Labels; tIdx(sampleIndexes) = -1;
%tIdx(tIdx~=-1) = 0;tIdx(tIdx == -1) = 1;
%tIdx = logical(tIdx);
tIdx = (SketchData.imdb.images.set == 3);
clear SketchData;
DatasetSize = size(Features,1);
[ImageSampleCount,ImageUniqueLabel] = hist(Labels,unique(Labels));
[IDX,D] = knnsearch(Features,Features(tIdx,:),...
    'k',DatasetSize,'distance','euclidean');

QueryLabels = Labels(tIdx);

PrecisionArray=[];RecallArray=[];
for i=1:size(IDX,1)
    disp(i);
    thisLabel = QueryLabels(i);
    cc =0;
    pr = [];re=[];
    for j =1:size(IDX,2)
        if (Labels(IDX(i,j)) == thisLabel)
            cc=cc+1;
        end
        pr(j) = cc/j;re(j) = cc/ImageSampleCount(find(thisLabel == ImageUniqueLabel));
        % pr(j) = cc;re(j) = cc/ImageSampleCount(thisSketchLabel);
        
    end
    PrecisionArray(i,:) = pr; RecallArray(i,:) = re;
end
Precision = mean(PrecisionArray,1);Recall = mean(RecallArray,1);
disp(Precision);disp(Recall);
save(['PrecisionRecall_' age '_' features '.mat'],'Precision','Recall');
%plot(Recall,Precision);title(features);savefig([features '.png']);close all;